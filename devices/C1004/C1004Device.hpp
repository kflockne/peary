/**
 * Caribou Device implementation for C1004
 */

#ifndef DEVICE_C1004_H
#define DEVICE_C1004_H

#include <string>
#include <vector>
#include "device/CaribouDevice.hpp"
#include "interfaces/SPI_BUS/spi_bus.hpp"

#include "C1004Defaults.hpp"

namespace caribou {

  /** C1004 Device class definition
   *
   *  this class implements the required functionality to operate C1004 chips via the
   *  Caribou device class interface.
   */
  class C1004Device : public CaribouDevice<iface_spi_bus<8, 8, 1, 1>> {

  public:
    C1004Device(const caribou::Configuration config);
    ~C1004Device();

    /** Initializer function for C1004
     */
    void configure() override;

    /** Turn on the power supply for the C1004 chip
     */
    void powerUp() override;

    /** Turn off the C1004 power
     */
    void powerDown() override;

    /** Start the data acquisition
     */
    void daqStart() override;

    /** Stop the data acquisition
     */
    void daqStop() override{};

    /**
     */
    std::vector<uintptr_t> getRawData() override;

    /**
     */
    pearydata getData() override;

    void setSpecialRegister(std::string name, uintptr_t value) override;
    uintptr_t getSpecialRegister(std::string name) override;

    // Reset the chip
    // The reset signal is asserted for ~1us
    void reset() override;
  };

} // namespace caribou

#endif /* DEVICE_C1004_H */
