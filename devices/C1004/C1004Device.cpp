/**
 * Caribou Device implementation for C1004
 */

#include "C1004Device.hpp"
#include <cmath>
#include "utils/log.hpp"

using namespace caribou;

C1004Device::C1004Device(const caribou::Configuration config)
    : CaribouDevice("falconboard", config, std::string(DEFAULT_DEVICEPATH)) {

  // Set up periphery
  _periphery.add("vbext_tdc", falconboard::BIAS_1);
  _periphery.add("vbext_vco", falconboard::BIAS_2);
  _periphery.add("tdc_ref", falconboard::BIAS_3);
  _periphery.add("veb", falconboard::BIAS_4);
  _periphery.add("vq", falconboard::BIAS_5);
  _periphery.add("vpu", falconboard::BIAS_6);
  _periphery.add("vwin", falconboard::BIAS_7);
  _periphery.add("vop", falconboard::BIAS_8);
  _periphery.add("vco_bias", falconboard::BIAS_9);

  // Add the register definitions to the dictionary for convenient lookup of names:
  _registers.add(C1004_REGISTERS);

  // Add memory pages to the dictionary:
  _memory.add(C1004_MEMORY);

  // keep C1004 in reset, bypass disabled
  setMemory("reset", 0);
}

void C1004Device::configure() {
  LOG(INFO) << "Configuring";
  reset();
  mDelay(10);

  CaribouDevice<iface_spi_bus<8, 8, 1, 1>>::configure();
}

void C1004Device::reset() {
  LOG(DEBUG) << "Resetting";

  // assert reset:
  setMemory("reset", getMemory("reset") & ~(C1004_CONTROL_RST_MASK));
  usleep(1);
  // deny reset:
  setMemory("reset", getMemory("reset") | C1004_CONTROL_RST_MASK);
}

C1004Device::~C1004Device() {

  LOG(INFO) << "Shutdown, delete device.";
  powerOff();
}

void C1004Device::powerUp() {
  LOG(INFO) << "Powering up";

  LOG(DEBUG) << "VBEXT_TDC";
  this->setVoltage("vbext_tdc", _config.Get("vbext_tdc", C1004_VBEXT_TDC));
  this->switchOn("vbext_tdc");

  LOG(DEBUG) << "VBEXT_VCO";
  this->setVoltage("vbext_vco", _config.Get("vbext_vco", C1004_VBEXT_VCO));
  this->switchOn("vbext_vco");

  LOG(DEBUG) << "TDC_REF";
  this->setVoltage("tdc_ref", _config.Get("tdc_ref", C1004_TDC_REF));
  this->switchOn("tdc_ref");

  LOG(DEBUG) << "VEB";
  this->setVoltage("veb", _config.Get("veb", C1004_VEB));
  this->switchOn("veb");

  LOG(DEBUG) << "VQ";
  this->setVoltage("vq", _config.Get("vq", C1004_VQ));
  this->switchOn("vq");

  LOG(DEBUG) << "VPU";
  this->setVoltage("vpu", _config.Get("vpu", C1004_VPU));
  this->switchOn("vpu");

  LOG(DEBUG) << "VWIN";
  this->setVoltage("vwin", _config.Get("vwin", C1004_VWIN));
  this->switchOn("vwin");

  LOG(DEBUG) << "VOP";
  this->setVoltage("vop", _config.Get("vop", C1004_VOP));
  this->switchOn("vop");

  LOG(DEBUG) << "VCO_BIAS";
  this->setVoltage("vco_bias", _config.Get("vco_bias", C1004_VCO_BIAS));
  this->switchOn("vco_bias");
}

void C1004Device::powerDown() {
  LOG(INFO) << "Power off";

  LOG(DEBUG) << "Power off VBEXT_TDC";
  this->switchOff("vbext_tdc");

  LOG(DEBUG) << "Power off VBEXT_VCO";
  this->switchOff("vbext_vco");

  LOG(DEBUG) << "Power off TDC_REF";
  this->switchOff("tdc_ref");

  LOG(DEBUG) << "Power off VEB";
  this->switchOff("veb");

  LOG(DEBUG) << "Power off VQ";
  this->switchOff("vq");

  LOG(DEBUG) << "Power off VPU";
  this->switchOff("vpu");

  LOG(DEBUG) << "Power off VWIN";
  this->switchOff("vwin");

  LOG(DEBUG) << "Power off VOP";
  this->switchOff("vop");

  LOG(DEBUG) << "Power off VCO_BIAS";
  this->switchOff("vco_bias");
}

void C1004Device::daqStart() {
  // Prepare chip for data acquisition
}

pearydata C1004Device::getData() {
  throw FirmwareException("Functionality not implemented.");
}

std::vector<uintptr_t> C1004Device::getRawData() {
  throw FirmwareException("Functionality not implemented.");
}

void C1004Device::setSpecialRegister(std::string name, uintptr_t value) {
  if(name == "ren") {
    uint8_t msb;
    uint8_t lsb;

    if(value > pow(2, 10) - 1) {
      LOG(WARNING) << "ren is limited to " << pow(2, 10) - 1 << " value, skipping";
      return;
    }
    msb = value >> 8;
    lsb = value & 0xFF;

    LOG(DEBUG) << "ren lookup: " << value << " = " << static_cast<int>(msb) << "-" << static_cast<int>(lsb);
    // Set the values
    this->setRegister("ren_lsb", msb);
    this->setRegister("ren_msb", lsb);
  }

  else if(name == "readout_select_column") {
    if(value > pow(2, 32) - 1) {
      LOG(WARNING) << "readout_select_column is limited to " << pow(2, 32) - 1 << " value, skipping";
      return;
    }

    LOG(DEBUG) << "readout_select_column: " << value;

    // Set the values
    this->setRegister("readout_select_column_0", value & 0xFF);
    this->setRegister("readout_select_column_1", static_cast<uint8_t>((value >> 8) & 0xFF));
    this->setRegister("readout_select_column_2", static_cast<uint8_t>((value >> 16) & 0xFF));
    this->setRegister("readout_select_column_3", static_cast<uint8_t>((value >> 24) & 0xFF));
  } else {
    throw RegisterInvalid("Unknown register with \"special\" flag: " + name);
  }
}

uintptr_t C1004Device::getSpecialRegister(std::string name) {
  if(name == "ren") {
    uint8_t msb = static_cast<uint8_t>(this->getRegister("ren_msb"));
    uint8_t lsb = static_cast<uint8_t>(this->getRegister("ren_lsb"));
    return (msb << 8) | lsb;
  } else if(name == "readout_select_column") {
    return (static_cast<uint8_t>(this->getRegister("readout_select_column_3")) << 24 |
            static_cast<uint8_t>(this->getRegister("readout_select_column_2")) << 16 |
            static_cast<uint8_t>(this->getRegister("readout_select_column_1")) << 8 |
            static_cast<uint8_t>(this->getRegister("readout_select_column_0")));
  } else {
    throw RegisterInvalid("Unknown register with \"special\" flag: " + name);
  }
}
