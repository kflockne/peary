#ifndef CARIBOU_HAL_SPI_BUS_H
#define CARIBOU_HAL_SPI_BUS_H

#include "interfaces/SPI/spi.hpp"

namespace caribou {

  /* SPI bus interface class
   * In this implementation 2 type of the SPI frames are considered: read and write frame.
   * The type of the frame is distinguished by the most significant bit of the SPI transaction on MOSI lines.
   * The polairity of this bit is distinguished by write strobe (WS) parameter of the template.
   * The frame type bit in the SPI frame is followed then by address of the register to be accessed and data.
   * The maximum width of SPI frame suppoprted by the class is limited by the width of uintmax_t type.
   *
   * E.g. ADDRESS_BITS = 8, DATA_BITS = 8, WRITE_STROBE = 1
   *
   * Read operation:
   *
   *         1b    8bit     8bit
   * MOSI | !WS |  ADDR  |    -  |
   * MISO | -   |    -   |  DATA |
   *
   * Write operation:
   *
   *         1b    8bit     8bit
   * MOSI | WS  |  ADDR  |  DATA  |
   * MISO |   - |    -   |   -    |
   *
   * As most of the SPI drivers support properly on 8 bits_per_word size, the actual SPI transaction has length
   * which is multiple of 8-bits. ALIGN_MSB defines whether the read/write operatio is aligned to MSB or LSB.
   * for the previous example, ALIGN_MSB=1 would correspond to the actual SPI transaction:
   *
   * Read/write operation:
   *         1b    8bit     8bit        7bit
   * MOSI | WS  |  ADDR  |  (DATA)  | PADDING
   * MISO |   - |    -   |  (DATA)  | PADDING
   *
   * While ALIGN_MSB=0 would produce:
   *
   * Read/write operation:
   *         7b      1b    8bit      8bit
   * MOSI PADDING | WS  |  ADDR  |  (DATA)  |
   * MISO PADDING |   - |    -   |  (DATA)  |
   *
   */
  template <uint8_t ADDRESS_BITS, uint8_t DATA_BITS, bool WS = 1, bool ALIGN_MSB = 0>
  class iface_spi_bus : public iface_spi {

  protected:
    // Mask constants used by the class
    const spi_reg_t ADDRESS_MASK = (1 << ADDRESS_BITS) - 1;
    const spi_t DATA_MASK = (1 << DATA_BITS) - 1;
    // Minimum length of the actual SPI frame aligned to 8bits, which can accomodate data
    const uint32_t LENGTH = (1 + ADDRESS_BITS + DATA_BITS) / 8 + ((1 + ADDRESS_BITS + DATA_BITS) % 8 != 0);

    // Default constructor: private (only created by InterfaceManager)
    // It can throw DeviceException
    iface_spi_bus(std::string const& device_path) : iface_spi(device_path){};

    virtual ~iface_spi_bus(){};

    GENERATE_FRIENDS()

    std::pair<spi_reg_t, spi_t> write(const spi_address_t& address, const std::pair<spi_reg_t, spi_t>& data);
    std::vector<std::pair<spi_reg_t, spi_t>> write(const spi_address_t& address,
                                                   const std::vector<std::pair<spi_reg_t, spi_t>>& data);
    std::vector<spi_t> read(const spi_address_t& address, const spi_reg_t reg, const unsigned int length = 1);

    // SPI access
    // rw = 0 read access
    // rw = 1 write acceess
    std::pair<spi_reg_t, spi_t> access(const bool rw, const spi_address_t& address, const std::pair<spi_reg_t, spi_t>& data);

    // Unused constructor
    iface_spi_bus() = delete;

    // only this function can create the interface
    friend iface_spi_bus<ADDRESS_BITS, DATA_BITS, WS, ALIGN_MSB>&
    InterfaceManager::getInterface<iface_spi_bus<ADDRESS_BITS, DATA_BITS, WS, ALIGN_MSB>>(std::string const&);
  }; // class iface_spi_bus

} // namespace caribou

#include "spi_bus.tcc"

#endif
