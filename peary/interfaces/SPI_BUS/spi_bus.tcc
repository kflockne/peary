/**
 * Caribou SPI interface class implementation
 */

#include <cstring>
#include <utility>

// OS SPI support
#include <fcntl.h>
#include <linux/spi/spidev.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include "utils/log.hpp"
#include "utils/utils.hpp"

using namespace caribou;

template <uint8_t ADDRESS_BITS, uint8_t DATA_BITS, bool WS, bool ALIGN_MSB>
std::pair<spi_reg_t, spi_t> iface_spi_bus<ADDRESS_BITS, DATA_BITS, WS, ALIGN_MSB>::access(
  const bool rw, const spi_address_t&, const std::pair<spi_reg_t, spi_t>& data) {

  std::lock_guard<std::mutex> lock(mutex);
  uintmax_t shift;
  std::array<uint8_t, 1 + sizeof(spi_reg_t) + sizeof(spi_t)> _data;
  std::pair<spi_reg_t, spi_t> rx;

#ifndef SPI_BUS_EMULATED

  if(ALIGN_MSB) {
    shift = (static_cast<uintmax_t>(rw ? WS : !WS) << (sizeof(shift) * 8 - 1)) |
            (static_cast<uintmax_t>(data.first & ADDRESS_MASK) << (sizeof(shift) * 8 - 1 - ADDRESS_BITS)) |
            (static_cast<uintmax_t>(data.second & DATA_MASK) << (sizeof(shift) * 8 - 1 - ADDRESS_BITS - DATA_BITS));

    // copy data to the char array
    // shifting assure endianess on different machines
    for(unsigned int i = 0; i < LENGTH; i++)
      _data[i] = (shift >> (sizeof(shift) - 1 - i) * 8) & 0xFF;
  } else {
    shift = ((rw ? WS : !WS) << (ADDRESS_BITS + DATA_BITS)) | ((data.first & ADDRESS_MASK) << DATA_BITS) |
            (data.second & DATA_MASK);

    // copy data to the char array
    // shifting assure endianess on different machines
    for(unsigned int i = 0; i < LENGTH; i++)
      _data[i] = (shift >> (LENGTH - 1 - i) * 8) & 0xFF;
  }

  spi_ioc_transfer tr = spi_ioc_transfer();
  tr.tx_buf = (uintptr_t)_data.data();
  tr.rx_buf = (uintptr_t)_data.data();
  tr.len = LENGTH;

  if(ioctl(spiDesc, SPI_IOC_MESSAGE(1), &tr) < 1) {
    throw CommunicationError("Failed to access device " + devicePath() + ": " + std::strerror(errno));
  }

  shift = 0;

  if(ALIGN_MSB) {
    for(unsigned int i = 0; i < LENGTH; i++)
      shift |= (static_cast<uintmax_t>(_data[i]) << (sizeof(shift) - 1 - i) * 8);
    rx = std::make_pair(static_cast<spi_reg_t>((shift >> (sizeof(shift) * 8 - 1 - ADDRESS_BITS)) & ADDRESS_MASK),
                        static_cast<spi_t>((shift >> (sizeof(shift) * 8 - 1 - ADDRESS_BITS - DATA_BITS)) & DATA_MASK));
  } else {
    for(unsigned int i = 0; i < LENGTH; i++)
      shift |= (static_cast<uintmax_t>(_data[i]) << (LENGTH - 1 - i) * 8);

    rx = std::make_pair(static_cast<spi_reg_t>((shift >> DATA_BITS) & ADDRESS_MASK), static_cast<spi_t>(shift & DATA_MASK));
  }

#endif

  LOG(TRACE) << "SPI device " << devicePath() << ": Register " << to_hex_string(data.first) << " Wrote data \""
             << to_hex_string(data.second) << "\" Read data \"" << to_hex_string(rx.second) << "\"";
  return rx;
}

template <uint8_t ADDRESS_BITS, uint8_t DATA_BITS, bool WS, bool ALIGN_MSB>
std::pair<spi_reg_t, spi_t>
iface_spi_bus<ADDRESS_BITS, DATA_BITS, WS, ALIGN_MSB>::write(const spi_address_t& address,
                                                             const std::pair<spi_reg_t, spi_t>& data) {
  return access(1, address, data);
}

template <uint8_t ADDRESS_BITS, uint8_t DATA_BITS, bool WS, bool ALIGN_MSB>
std::vector<std::pair<spi_reg_t, spi_t>>
iface_spi_bus<ADDRESS_BITS, DATA_BITS, WS, ALIGN_MSB>::write(const spi_address_t& address,
                                                             const std::vector<std::pair<spi_reg_t, spi_t>>& data) {
  std::vector<std::pair<spi_reg_t, spi_t>> rx;
  rx.reserve(data.size());
  for(auto& _data : data)
    rx.push_back(write(address, _data));

  return rx;
}

template <uint8_t ADDRESS_BITS, uint8_t DATA_BITS, bool WS, bool ALIGN_MSB>
std::vector<spi_t> iface_spi_bus<ADDRESS_BITS, DATA_BITS, WS, ALIGN_MSB>::read(const spi_address_t& address,
                                                                               const spi_reg_t reg,
                                                                               const unsigned int length) {

  std::vector<spi_t> rx;

  for(unsigned int i = 0; i < length; i++)
    rx.push_back(access(0, address, std::make_pair(reg, 0)).second);

  return rx;
}
