#ifndef CARIBOU_HAL_IIO_HPP
#define CARIBOU_HAL_IIO_HPP

#include <map>
#ifndef IIO_EMULATED
#include "iio.h"
#endif

namespace caribou {

  typedef component_dac_t iio_address_t;
  typedef uint8_t iio_reg_t;
  typedef struct {
    // command's type:
    // - Set value in the device
    // - enable the device
    enum { ENABLE, SET } cmd;

    union {
      // enable/disable the channel
      bool enable;

      /** Voltage/Current of a given channel in SI Volts/Ampers
       */
      double value = 0;
    };
  } iio_t;

  // The interface to serve simple devices (e.g. DACs) thorugh IIO library
  template <const char* chan_type> class iface_iio : public Interface<iio_address_t, iio_reg_t, iio_t> {

  protected:
    // Default constructor: private (only created by InterfaceManager)
    // It can throw DeviceException
    iface_iio(std::string const& device_path);

    virtual ~iface_iio();

#ifndef IIO_EMULATED

    // IIO context used by the interface
    struct iio_context* iio_ctx;

    // Map of all the channels used by the interface
    std::map<std::string, iio_channel*> channels;

    iio_channel* getIIOChannel(const iio_address_t dev);

    void updateIIOChannel(const iio_address_t dev);

#endif

    GENERATE_FRIENDS()

    virtual iio_t write(const iio_address_t&, const iio_t&) override;

    // Unused constructor
    iface_iio() = delete;

    // only this function can create the interface
    friend iface_iio<chan_type>& InterfaceManager::getInterface<iface_iio<chan_type>>(std::string const&);
  };

} // namespace caribou

#include "iio.tcc"

#endif
