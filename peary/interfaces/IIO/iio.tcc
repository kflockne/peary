/**
 * Caribou IIO interface class implementation
 */

using namespace caribou;

#ifndef IIO_EMULATED

struct iioChannelPrivateData {
  /** The channel is enabled
   */
  bool on = true;

  /** Voltage/Current of a given channel in SI Volts
   */
  double value = 0;
};

template <const char* chan_type> iface_iio<chan_type>::iface_iio(std::string const& device_path) : Interface(device_path) {

  // Initial IIO context
  LOG(DEBUG) << "Creating IIO context";
  iio_ctx = iio_create_context_from_uri(device_path.c_str());

  if(iio_ctx == NULL)
    throw DeviceException("Failed to create IIO context");

  LOG(DEBUG) << "IIO context contains " << iio_context_get_devices_count(iio_ctx) << " devices";
}

template <const char* chan_type> iface_iio<chan_type>::~iface_iio() {

  LOG(DEBUG) << "Deleting IIO channels";
  for(auto it = channels.begin(); it != channels.end(); it++)
    delete static_cast<iioChannelPrivateData*>(iio_channel_get_data(it->second));

  LOG(DEBUG) << "Destroying IIO context";
  iio_context_destroy(iio_ctx);
}

template <const char* chan_type> iio_t iface_iio<chan_type>::write(const iio_address_t& regulator, const iio_t& data) {

  switch(data.cmd) {
  case iio_t::ENABLE:
    static_cast<iioChannelPrivateData*>(iio_channel_get_data(getIIOChannel(regulator)))->on = data.enable;
    updateIIOChannel(regulator);
    break;

  case iio_t::SET:
    static_cast<iioChannelPrivateData*>(iio_channel_get_data(getIIOChannel(regulator)))->value = data.value;

    if(static_cast<iioChannelPrivateData*>(iio_channel_get_data(getIIOChannel(regulator)))->on)
      updateIIOChannel(regulator);
    break;
  }

  return iio_t();
}

template <const char* chan_type> iio_channel* iface_iio<chan_type>::getIIOChannel(const iio_address_t dev) {
  try {
    return channels.at(dev.name());
  }
  // such a channel has not been opened yet
  catch(const std::out_of_range&) {

    const std::string iio_id = "iio:device" + std::to_string(dev.dacaddress());
    const std::string chan_id = chan_type + std::to_string(dev.dacoutput());
    const iio_device* iio_dev = iio_context_find_device(iio_ctx, iio_id.c_str());
    iio_channel* channel;

    if(iio_dev == NULL)
      throw ConfigInvalid("Device " + iio_id + " not found");

    channel = iio_device_find_channel(iio_dev, chan_id.c_str(), true);
    if(channel == NULL)
      throw ConfigInvalid("Channel " + std::string(chan_id) + " on device " + iio_id + " not found");

    iio_channel_set_data(channel, static_cast<void*>(new iioChannelPrivateData));

    channels[dev.name()] = channel;

    return channels[dev.name()];
  }
}

template <const char* chan_type> void iface_iio<chan_type>::updateIIOChannel(const iio_address_t dev) {
  iio_channel* chn = getIIOChannel(dev);
  iioChannelPrivateData* chn_status = static_cast<iioChannelPrivateData*>(iio_channel_get_data(chn));

  if(chn_status->on) {
    double scale;
    double value;

    if(iio_channel_attr_read_double(chn, "scale", &scale))
      throw DeviceException("Failed to obtain " + std::string(chan_type) + " scale for " + dev.name() + "(" +
                            std::strerror(errno) + ")");

    value = chn_status->value * 1000 / scale;

    LOG(DEBUG) << "Powering up channel " << iio_channel_get_id(chn) << " at "
               << to_hex_string(dev.dacaddress()) + " with raw value " << value;

    if(iio_channel_attr_write_double(chn, "raw", value))
      throw DeviceException("Failed to set " + dev.name() + " " + chan_type + " (" + std::strerror(errno) + ")");
  } else {
    LOG(DEBUG) << "Powering down channel " << iio_channel_get_id(chn) << " at " << to_hex_string(dev.dacaddress());
    if(iio_channel_attr_write_bool(chn, "powerdown", true))
      throw DeviceException("Failed to powerdown " + dev.name() + "(" + std::strerror(errno) + ")");
  }
}

#else

///////////////////////////
// Emulation implementation
///////////////////////////

template <const char* chan_type> iface_iio<chan_type>::iface_iio(std::string const& device_path) : Interface(device_path) {}

template <const char* chan_type> iface_iio<chan_type>::~iface_iio() {}

template <const char* chan_type> iio_t iface_iio<chan_type>::write(const iio_address_t& regulator, const iio_t& data) {

  switch(data.cmd) {
  case iio_t::ENABLE:
    LOG(DEBUG) << "(emulator) Powering" << (data.enable ? " up " : " down ") << regulator.name();
    break;

  case iio_t::SET:
    LOG(DEBUG) << "(emulator) At next power up " + std::string(chan_type) + " of " + regulator.name() +
                    " will be set to value " + to_string(data.value);
    break;
  }

  return iio_t();
}

#endif
