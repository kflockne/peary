#ifndef CARIBOU_HAL_H
#define CARIBOU_HAL_H

#include <cstdint>
#include <map>
#include <string>
#include <tuple>
#include <typeinfo>
#include <vector>

#include "utils/constants.hpp"
#include "utils/exceptions.hpp"
#include "utils/log.hpp"
#include "utils/utils.hpp"

#include "interfaces/Interface.hpp"
#include "interfaces/InterfaceManager.hpp"

#include "interfaces/I2C/i2c.hpp"
#include "interfaces/Memory/memory.hpp"

#include "Si5345-RevB-CLKOFF-Registers.h"

namespace caribou {

  // Base class to share configuration variable between different caribouHAL specialisations
  class caribouHALbase {
  public:
    caribouHALbase(const std::type_info& t) : fCompleteType(t) {}
    // Call Initialize only in the most derived class
    void InitializeIfComplete() {
      if(typeid(*this) == fCompleteType)
        Initialize();
    }
    // Call Finalize only in the most derived class
    void FinalizeIfComplete() {
      if(typeid(*this) == fCompleteType)
        Finalize();
    }
    virtual void Initialize() = 0;
    virtual void Finalize() = 0;

  protected:
    // General reset of the board done
    static bool generalResetDone;
    // Final type of the instance
    const std::type_info& fCompleteType;
  };

  template <typename T> class caribouHAL : public virtual caribouHALbase {

  public:
    /** Factory to dynamically create specialised HAL objects
     */
    static std::shared_ptr<caribouHAL<T>>
    Factory(const std::string& hal_class, const std::string& device_path, uintptr_t device_address);

    /** Default constructor for creating a new HAL instance
     */
    caribouHAL(std::string device_path, uintptr_t device_address);

    /** Default destructor
     */
    ~caribouHAL();

    /** Read and return the device identifier from the firmware
     */
    uint8_t getBoardID();

    /** Return human-readable string of firmware version and build timestamp
     */
    virtual std::string getFirmwareVersion() = 0;

    /** Read value from a firmware register
     *
     *  @param address : address of the register to be read
     */
    virtual uintptr_t getFirmwareRegister(uint16_t address) = 0;

    // Write data to a device which does not contain internal register
    // If readout is intergralpart of write operations, the read values a returned by function.
    typename T::data_type send(const typename T::data_type& data);

    // Write data to a device which does not contain internal register
    // If readout is intergralpart of write operations, the read values a returned by function.
    std::vector<typename T::data_type> send(const std::vector<typename T::data_type>& data);

    // Write data to a device containing internal registers
    // If readout is intergralpart of write operations, the read values a returned by function.
    std::pair<typename T::reg_type, typename T::data_type>
    send(const std::pair<typename T::reg_type, typename T::data_type>& data);

    // Write data to a device containing internal registers
    // If readout is intergralpart of write operations, the read values a returned by function.
    std::vector<typename T::data_type> send(const typename T::reg_type& reg, const std::vector<typename T::data_type>& data);

    // Write data to a device containing internal registers
    // If readout is intergralpart of write operations, the read values a returned by function.
    std::vector<std::pair<typename T::reg_type, typename T::data_type>>
    send(const std::vector<std::pair<typename T::reg_type, typename T::data_type>>& data);

    // Read data from a device which does not contain internal register
    std::vector<typename T::data_type> receive(const unsigned int length = 1);

    // Read data from a device containing internal registers
    std::vector<typename T::data_type> receive(const typename T::reg_type reg, const unsigned int length = 1);

    void writeMemory(memory_map mem, size_t offset, uintptr_t value);
    void writeMemory(memory_map mem, uintptr_t value);
    uintptr_t readMemory(memory_map mem, size_t offset);
    uintptr_t readMemory(memory_map mem);

    /** Read the temperature from the TMP101 device
     *
     *  Returns temperature in degree Celsius with a precision of 0.0625degC
     */
    virtual double readTemperature() = 0;

    /** Set bias voltage
     *
     *  The input parameter should be provided in SI Volts.
     *  The function sets the proper voltage in the DAC corresponding to the selected bias.
     *  The output of the DAC is not enabled.
     */
    virtual void setBiasRegulator(const BIAS_REGULATOR_T regulator, const double voltage) = 0;

    /** Enable/disable the voltage regulator
     *
     *  Enables or disables the output of the coresponding DAC.
     */
    virtual void powerBiasRegulator(const BIAS_REGULATOR_T regulator, const bool enable) = 0;

    /** Set output voltage of a voltage regulator
     *
     *  The input parameter should be provided in SI Volts and Amps.
     *  The function sets the proper reference voltage (PWR_ADJ_*) in a DAC
     *  corresponding to the regulator. It also configures the corresponding current/power monitor.
     *  The output of the DAC is not enabled.
     */
    virtual void
    setVoltageRegulator(const VOLTAGE_REGULATOR_T regulator, const double voltage, const double maxExpectedCurrent = 3) = 0;

    /** Set the current source.
     * ~current~ is provided in uA.
     */
    virtual void setCurrentSource(const CURRENT_SOURCE_T source,
                                  const unsigned int current,
                                  const CURRENT_SOURCE_POLARITY_T polarity) = 0;

    /** Enable/disable the voltage regulator
     *
     *  If enable is true, the function enables first output
     *  of the coresponding DAC (PWR_ADJ_*). Afterwards, PWER_EN_* is asserted.
     *  If enable is false, the sequence is performed in the opposite direction.
     */
    virtual void powerVoltageRegulator(const VOLTAGE_REGULATOR_T regulator, const bool enable) = 0;

    /** Enables current source
     */
    virtual void powerCurrentSource(const CURRENT_SOURCE_T source, const bool enable) = 0;

    // The method disables all outputs of SI5345 jitter attenuator/clock multiplier using a table generated by
    // ClockBuilderPro
    virtual void disableSI5345() = 0;

    // The method sets SI5345 jitter attenuator/clock multiplier using a table generated by ClockBuilderPro
    virtual void configureSI5345(SI5345_REG_T const* const regs, const size_t length) = 0;

    // The method return true when SI5345 jitter attenuator/clock multiplier is locked
    virtual bool isLockedSI5345() = 0;

    // The method sets pulse parameters for an onboard pulser
    virtual void configurePulseParameters(const unsigned channel_mask,
                                          const uint32_t periods,
                                          const uint32_t time_active,
                                          const uint32_t time_idle,
                                          const double voltage) = 0;

    // The method configures the onboard pulser
    virtual void
    configurePulser(unsigned channel_mask, const bool ext_trigger, const bool ext_trig_edge, const bool idle_state) = 0;

    // The method starts the onboard pulser
    virtual void startPulser(unsigned channel_mask) = 0;

    // The method enables the onboard pulser
    virtual void enablePulser(unsigned channel_mask) = 0;

    // The method disables (stops) the onboard pulser
    virtual void disablePulser(unsigned channel_mask) = 0;

    // The method returns 4 status bits corresponding to 4 pulser channels if a channel is running
    virtual uint32_t getPulserRunning() = 0;

    // The returns the number of pulses that were done since a pulser channel was started for the specified channel.
    virtual uint32_t getPulseCount(const uint32_t channel) = 0;

    // The method measures current
    // It return value in SI A.
    virtual double measureCurrent(const VOLTAGE_REGULATOR_T regulator) = 0;

    // The method measures power
    // It return value in SI W.
    virtual double measurePower(const VOLTAGE_REGULATOR_T regulator) = 0;

    // The method measures voltage
    // It returns vale in SI V.
    virtual double measureVoltage(const VOLTAGE_REGULATOR_T regulator) = 0;

    virtual double readSlowADC(const SLOW_ADC_CHANNEL_T channel) = 0;

  protected:
    /** Initailization of the board
     */
    virtual void Initialize() override;

    /** Finalization of the board
     */
    virtual void Finalize() override;

    // It's not possible to power on/off DCDC converter by software
    virtual void setDCDCConverter(const DCDC_CONVERTER_T converter, const double voltage) = 0;

    /** Device path of the configured device
     */
    std::string _devpath;

    /** Address of the configured device
     */
    uintptr_t _devaddress;

    /** Set current/power monitor
     */
    virtual void setCurrentMonitor(const uint8_t device, const double maxExpectedCurrent) = 0;

    /** General reset of the board
     */
    virtual void generalReset();

  }; // class caribouHAL

} // namespace caribou

#include "HAL.tcc"

#endif /* CARIBOU_HAL_H */
