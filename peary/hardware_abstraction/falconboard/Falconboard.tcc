/**
 * Caribou Falconboard HAL class implementation
 */
#ifndef CARIBOU_FALCONBOARD_IMPL
#define CARIBOU_FALCONBOARD_IMPL

namespace caribou {
  namespace falconboard {

    template <typename T> const char Falconboard<T>::iio_chan_type[] = "voltage";

    template <typename T>
    Falconboard<T>::Falconboard(std::string device_path, uintptr_t device_address)
        : caribouHALbase(typeid(Falconboard<T>)), caribouHAL<T>(device_path, device_address) {

      // Log the firmware
      LOG(STATUS) << getFirmwareVersion();

      this->InitializeIfComplete();
    }

    template <typename T> Falconboard<T>::~Falconboard() {}

    template <typename T> std::string Falconboard<T>::getFirmwareVersion() {

      const uint32_t firmwareVersion = this->readMemory(reg_firmware);
      const uint8_t day = (firmwareVersion >> 27) & 0x1F;
      const uint8_t month = (firmwareVersion >> 23) & 0xF;
      const uint32_t year = 2000 + ((firmwareVersion >> 17) & 0x3F);
      const uint8_t hour = (firmwareVersion >> 12) & 0x1F;
      const uint8_t minute = (firmwareVersion >> 6) & 0x3F;
      const uint8_t second = firmwareVersion & 0x3F;

      std::stringstream s;
      s << "Firmware version: " << to_hex_string(firmwareVersion) << " (" << static_cast<int>(day) << "/"
        << static_cast<int>(month) << "/" << static_cast<int>(year) << " " << static_cast<int>(hour) << ":"
        << static_cast<int>(minute) << ":" << static_cast<int>(second) << ")";

      return s.str();
    }

    template <typename T> void Falconboard<T>::generalReset() {
      // Disable DAC outputs
      LOG(DEBUG) << "Disable all DACs channels";
      powerBiasRegulator(BIAS_1, false);
      powerBiasRegulator(BIAS_2, false);
      powerBiasRegulator(BIAS_3, false);
      powerBiasRegulator(BIAS_4, false);
      powerBiasRegulator(BIAS_5, false);
      powerBiasRegulator(BIAS_6, false);
      powerBiasRegulator(BIAS_7, false);
      powerBiasRegulator(BIAS_8, false);
      powerBiasRegulator(BIAS_9, false);
      powerBiasRegulator(BIAS_10, false);
      powerBiasRegulator(BIAS_11, false);
      powerBiasRegulator(BIAS_12, false);

      caribouHAL<T>::generalReset();
    }

    template <typename T> uintptr_t Falconboard<T>::getFirmwareRegister(uint16_t) {
      throw FirmwareException("Functionality not implemented.");
    }

    template <typename T> uint8_t Falconboard<T>::getBoardID() {

      throw FirmwareException("Functionality not implemented.");

      // LOG(DEBUG) << "Reading board ID from CaR EEPROM";
      // iface_i2c& myi2c = InterfaceManager::getInterface<iface_i2c>(BUS_I2C0);

      // // Read one word from memory address on the EEPROM:
      // // FIXME register address not set!
      // std::vector<uint8_t> data = myi2c.wordread(ADDR_EEPROM, 0x0, 1);

      // if(data.empty())
      //   throw CommunicationError("No data returned");
      // return data.front();
    }

    template <typename T> double Falconboard<T>::readTemperature() {
      throw FirmwareException("Functionality not implemented.");
    }

    template <typename T> void Falconboard<T>::setBiasRegulator(const BIAS_REGULATOR_T regulator, const double voltage) {
      LOG(DEBUG) << "Setting bias " << voltage << "V "
                 << "on " << regulator.name();

      // actual voltage to be set
      double voltage_ = voltage;

      // boosted DACs
      if(regulator.name().compare(BIAS_8.name()) == 0 || regulator.name().compare(BIAS_12.name()) == 0) {
        if(voltage > 30 || voltage < 0)
          throw ConfigInvalid("Trying to set bias regulator to " + std::to_string(voltage) + " V (range is 0-30 V)");

        voltage_ /= DAC_BOOST_AMPLIFICATION_RATIO;

      } else // regular DAC
        if(voltage > 2.5 || voltage < 0)
        throw ConfigInvalid("Trying to set bias regulator to " + std::to_string(voltage) + " V (range is 0-2.5 V)");

      iface_iio<iio_chan_type>& iio = InterfaceManager::getInterface<iface_iio<iio_chan_type>>("local:");
      iio_t iio_data;
      iio_data.cmd = iio_t::SET;
      iio_data.value = voltage_;
      iio.write(regulator, iio_data);
    }

    template <typename T> void Falconboard<T>::powerBiasRegulator(const BIAS_REGULATOR_T regulator, const bool enable) {
      LOG(DEBUG) << (enable ? "Powering up " : "Powering down ") << regulator.name();

      iface_iio<iio_chan_type>& iio = InterfaceManager::getInterface<iface_iio<iio_chan_type>>("local:");
      iio_t iio_data;
      iio_data.cmd = iio_t::ENABLE;
      iio_data.enable = enable;
      iio.write(regulator, iio_data);
    }

    template <typename T>
    void Falconboard<T>::setVoltageRegulator(const VOLTAGE_REGULATOR_T regulator,
                                             const double voltage,
                                             const double maxExpectedCurrent) {
      throw FirmwareException("Functionality not supported.");
    }

    template <typename T>
    void Falconboard<T>::powerVoltageRegulator(const VOLTAGE_REGULATOR_T regulator, const bool enable) {

      throw FirmwareException("Functionality not supported.");
    }

    template <typename T>
    void Falconboard<T>::setCurrentSource(const CURRENT_SOURCE_T source,
                                          const unsigned int current,
                                          const CURRENT_SOURCE_POLARITY_T polarity) {

      throw FirmwareException("Functionality not supported.");
    }

    template <typename T> void Falconboard<T>::powerCurrentSource(const CURRENT_SOURCE_T source, const bool enable) {
      throw FirmwareException("Functionality not supported.");
    }

    template <typename T> void Falconboard<T>::disableSI5345() { throw FirmwareException("Functionality not supported."); }

    template <typename T> void Falconboard<T>::configureSI5345(SI5345_REG_T const* const regs, const size_t length) {
      throw FirmwareException("Functionality not supported.");
    }

    template <typename T> bool Falconboard<T>::isLockedSI5345() { throw FirmwareException("Functionality not supported."); }

    template <typename T>
    void Falconboard<T>::configurePulser(unsigned channel_mask,
                                         const bool ext_trigger,
                                         const bool ext_trig_edge,
                                         const bool idle_state) {
      throw FirmwareException("Functionality not supported.");
    }

    template <typename T> void Falconboard<T>::startPulser(unsigned channel_mask) {
      throw FirmwareException("Functionality not supported.");
    }

    template <typename T> void Falconboard<T>::enablePulser(unsigned channel_mask) {
      throw FirmwareException("Functionality not supported.");
    }

    template <typename T> void Falconboard<T>::disablePulser(unsigned channel_mask) {
      throw FirmwareException("Functionality not supported.");
    }

    template <typename T> uint32_t Falconboard<T>::getPulserRunning() {
      throw FirmwareException("Functionality not supported.");
    };

    template <typename T> uint32_t Falconboard<T>::getPulseCount(const uint32_t channel) {
      throw FirmwareException("Functionality not supported.");
    }

    template <typename T>
    void Falconboard<T>::configurePulseParameters(const unsigned channel_mask,
                                                  const uint32_t periods,
                                                  const uint32_t time_active,
                                                  const uint32_t time_idle,
                                                  const double voltage) {

      throw FirmwareException("Functionality not supported.");
    }

    template <typename T> void Falconboard<T>::setCurrentMonitor(const uint8_t device, const double maxExpectedCurrent) {
      throw FirmwareException("Functionality not supported.");
    }

    template <typename T> void Falconboard<T>::setDCDCConverter(const DCDC_CONVERTER_T converter, const double voltage) {
      throw FirmwareException("Functionality not supported.");
    }

    template <typename T> double Falconboard<T>::measureCurrent(const VOLTAGE_REGULATOR_T regulator) {
      throw FirmwareException("Functionality not supported.");
    }

    template <typename T> double Falconboard<T>::measurePower(const VOLTAGE_REGULATOR_T regulator) {
      throw FirmwareException("Functionality not supported.");
    }

    template <typename T> double Falconboard<T>::measureVoltage(const VOLTAGE_REGULATOR_T regulator) {
      throw FirmwareException("Functionality not supported.");
    }

    template <typename T> double Falconboard<T>::readSlowADC(const SLOW_ADC_CHANNEL_T channel) {
      throw FirmwareException("Functionality not supported.");
    }

  } // namespace falconboard
} // namespace caribou

#endif /* CARIBOU_HAL_IMPL */
